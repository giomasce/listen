# Baby listener

A little and hacky script that listens to your sleeping
baby and warns you on Telegram if they cry. It works
good enough for me, but don't take it too seriously.

## Installation

* Get a spare Android phone. No need for root access.

* Install Termux and Termux:API. Apparently you have
to install both from the same store. I.e., either both
from Google or both from F-Droid. When they ask, give
them all permissions they want.

* Install some Termux packages:
```
pkg install termux-api ffmpeg git iproute2 python-pip
```

* Install some Python packages:
```
pip install requests
```

* Clone the repository:
```
git clone https://gitlab.com/giomasce/listen.git
```

* Enter the repository and create a sample configuration:
```
cd listen; cp config.ini.sample config.ini
```

* Create a Telegram bot, following the [official
documentation](https://core.telegram.org/bots#6-botfather),
then copy the bot token in the `config.ini` file.

* Get your Telegram contact id, for example querying
the `@RawInfoBot`, and copy it in the `config.ini` file.

* Write something to the bot. It doesn't matter what, the point is that a bot cannot contact you if you don't contact it before.

* Run the program!
```
./listen.py
```

* Use the `recipients` section in the `config.ini` file to specify alternative
recipients, that can be selected at runtime as in
```
./listen.py the_group
```

* Acquire the wakelock from Termux' notification. This
way the listener will not be stopped when Android
disables the screen.

* When you wish, stop it with Ctrl-C. You can also release
the wakelock.
