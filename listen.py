#!/usr/bin/env python3

import subprocess
import threading
import time
import datetime
import queue
import requests
import wave
import struct
import itertools
import json
import traceback
import configparser
import sys

RUNNING = True

record_queue = queue.Queue(maxsize=10)

def run_command(cmd):
    return subprocess.run(cmd, shell=True, check=True, capture_output=True)

def record_thread_main():
    count = 0
    while RUNNING:
        try:
            aac_filename = 'record/record{}.aac'.format(count)
            wav_filename = 'record/record{}.wav'.format(count)
            timestamp = datetime.datetime.now()
            run_command('termux-microphone-record -f {} -l 20'.format(aac_filename))
            print('Recording {}'.format(aac_filename))
            time.sleep(5)
            run_command('termux-microphone-record -q')
            print('Recorded {}'.format(aac_filename))
            item = {
                'timestamp': timestamp,
                'aac_filename': aac_filename,
                'wav_filename': wav_filename,
            }
            record_queue.put(item)
        except:
            print('Exception raised in record thread')
            traceback.print_exc()
        count += 1

def list_local_ips():
    ips = run_command("ip addr | grep 'inet' | cut -d' ' -f6 | cut -d/ -f1").stdout.decode('ascii').splitlines()
    ips = [ip for ip in ips if not ip.startswith('fe80:') and not ip.startswith('127.') and not ip == '::1']
    return ips

def format_battery_status():
    cmd = run_command('termux-battery-status')
    data = json.loads(cmd.stdout)
    return f'{data["percentage"]}%, {data["status"].lower()}'

def format_message(item, last_loud_timestamp):
    lines = []
    lines.append(f'Timestamp: {item["timestamp"].strftime("%H:%M:%S")}')
    lines.append(f'Max loudness: {item["max_frame"]}')
    lines.append(f'Local IPs: {", ".join(list_local_ips())}')
    lines.append(f'Battery: {format_battery_status()}')
    if last_loud_timestamp is not None:
        diff = item['timestamp'] - last_loud_timestamp
        tot_secs = int(diff.total_seconds())
        minutes = tot_secs // 60
        seconds = tot_secs % 60
        lines.append(f'Silent for: {minutes}:{seconds:02}')
    return '\n'.join(lines)

def upload_audio(item):
    global config
    url = 'https://api.telegram.org/bot' + config['listen']['bot_token'] + '/sendAudio'
    data = {
        'chat_id': config['listen']['recipient'],
        'caption': format_message(item, None),
    }
    files = {
        'audio': ('audio.aac',
                  open(item['aac_filename'], 'rb'),
                  'video/mp4'),
    }
    r = requests.post(url, data=data, files=files)
    r.raise_for_status()

def update_message(item, last_message_id, last_loud_timestamp):
    text = format_message(item, last_loud_timestamp)
    if last_message_id is None:
        url = 'https://api.telegram.org/bot' + config['listen']['bot_token'] + '/sendMessage'
        data = {
            'chat_id': config['listen']['recipient'],
            'disable_notification': True,
            'text': text,
        }
    else:
        url = 'https://api.telegram.org/bot' + config['listen']['bot_token'] + '/editMessageText'
        data = {
            'chat_id': config['listen']['recipient'],
            'message_id': last_message_id,
            'text': text,
        }
    r = requests.post(url, data=data)
    try:
        r.raise_for_status()
    except requests.HTTPError as exc:
        if exc.response.status_code == 400 and last_message_id is not None:
            # Maybe the message we are trying to edit was deleted?
            # Attempt to send a brand new one:
            return update_message(item, None, last_loud_timestamp)
        raise exc
    ret = json.loads(r.text)
    if last_message_id is None:
        if ret['ok']:
            return ret['result']['message_id']
    else:
        return last_message_id

def load_wave_frames(item):
    wav = wave.open(item['wav_filename'], 'rb')
    assert wav.getframerate() == 8000
    assert wav.getsampwidth() == 2
    assert wav.getnchannels() == 1
    frames = []
    while True:
        packet = [x[0] for x in struct.iter_unpack('<h', wav.readframes(1000))]
        if len(packet) == 0:
            break
        frames.append(packet)
    item['wave_frames'] = list(itertools.chain.from_iterable(frames))

def analyze_record(item):
    max_frame = max([abs(x) for x in item['wave_frames']])
    item['max_frame'] = max_frame
    return max_frame >= int(config['listen']['sound_threshold'])

def process_thread_main():
    last_message_id = None
    last_loud_timestamp = None
    while RUNNING or not record_queue.empty():
        try:
            item = record_queue.get()
            print('Transcoding {} to {}'.format(item['aac_filename'], item['wav_filename']))
            run_command('ffmpeg -i {} -vn -acodec pcm_s16le -ac 1 -ar 8000 -f wav -y {}'.format(item['aac_filename'], item['wav_filename']))
            print('Transcoded {} to {}'.format(item['aac_filename'], item['wav_filename']))
            load_wave_frames(item)
            print('Loaded {} frames from {}'.format(len(item['wave_frames']), item['wav_filename']))
            item['interesting'] = analyze_record(item)
            if item['interesting']:
                print('Record {} is loud, uploading'.format(item['wav_filename']))
                upload_audio(item)
                last_message_id = None
                last_loud_timestamp = item['timestamp']
            else:
                print('Record {} is not loud, ignoring'.format(item['wav_filename']))
                last_message_id = update_message(item, last_message_id, last_loud_timestamp)
        except:
            print('Exception raised in process thread')
            traceback.print_exc()
        run_command('rm {} {} || true'.format(item['aac_filename'], item['wav_filename']))

def main():
    global config
    config = configparser.ConfigParser()
    config.read('config.ini')

    if len(sys.argv) > 1:
        # Recipient specified in command line (from menu in config.ini):
        recipient = sys.argv[1]
        config['listen']['recipient'] = config['recipients'][recipient]

    run_command('rm -fr record')
    run_command('mkdir record')
    record_thread = threading.Thread(target=record_thread_main)
    record_thread.start()
    process_thread = threading.Thread(target=process_thread_main)
    process_thread.start()
    # try:
    #     while True:
    #         time.sleep(10)
    # except KeyboardInterrupt:
    #     print('Exiting')
    #     RUNNING = False

if __name__ == '__main__':
    main()
